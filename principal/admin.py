# encoding: utf-8
from django.contrib import admin
from principal import models
from django.db.models.base import ModelBase
# Registra automaticamente todos os Models na interface Administrativa do Django
model_list = [m for m in dir(models) if isinstance(models.__getattribute__(m), ModelBase)]

for model_class in model_list:
    klass = models.__getattribute__(model_class)
    try:
        admin.site.register(klass)
    except:
        pass
