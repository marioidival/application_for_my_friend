# encoding: utf-8
from django.db import models


class Todo(models.Model):
    nome_lista = models.CharField(max_length=100)
    
    def __unicode__(self):
        return "%s" %(self.nome_lista)

class Criador(models.Model):
    nome = models.CharField(max_length=50)
    sobrenome = models.CharField(max_length=100)
    profissao = models.CharField(max_length=100, blank=True)
    experiencia = models.IntegerField(blank=True)

    def __unicode__(self):
        return "%s %s = %s" %(self.nome, self.sobrenome, self.profissao)

class TodoList(models.Model):
    lista = models.ForeignKey(Todo)
    atividade = models.CharField(max_length=100)
    data_inicio = models.DateField()
    descricao = models.TextField()
    criador = models.ForeignKey(Criador)

    def __unicode__(self):
        return "%s - %s - %s" %(self.criador, self.data_inicio, self.atividade)


