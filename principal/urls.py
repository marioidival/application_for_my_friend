from django.conf.urls import patterns, include, url
from principal import views

urlpatterns = patterns('',
        url(r'^$', views.index, name='index'),

        url(r'^todo/$', views.todos, name='todolist'),
        url(r'^todo/criar', views.todo_criar, name='criar_todo'),
        url(r'^todo/(?P<todo_id>\d+)/detalhe', views.todo_detalhe, name='todo_detalhe'),
        url(r'^todo/deletar/(?P<todo_id>\d+)', views.todo_deletar, name='todo_deletar'),
        
        url(r'^todolist/$', views.todolists, name='todolistas'),
        url(r'^todolist/criar', views.todolist_criar, name='criar_todolist'),
        url(r'^todolist/(?P<todolist_id>\d+)/detalhe', views.todolist_detalhe, name='todolist_detalhe'),
        url(r'^todolist/deletar/(?P<todolist_id>\d+)', views.todolist_deletar, name='todolist_deletar'),

        url(r'^criador/$', views.criadores, name='criadores'),
        url(r'^criador/criar', views.criador_criar, name='criar_criador'),
        url(r'^criador/(?P<criador_id>\d+)/detalhe', views.criador_detalhe, name='criador_detalhe'),
        url(r'^criador/deletar/(?P<criador_id>\d+)', views.criador_deletar, name='criador_deletar'),



        )
