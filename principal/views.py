from django.http import HttpResponse
from django.shortcuts import render_to_response, redirect, get_object_or_404
from django.template import RequestContext
from principal.models import Todo, TodoList, Criador 
from principal.forms import *

def index(request):
    return render_to_response("base.html", {}, context_instance=RequestContext(request)) 

def todos(request):
    obj = Todo.objects.all()
    return render_to_response('todos.html', { 'obj' : obj }, 
            context_instance=RequestContext(request))

def todo_criar(request):
    todo_form = TodoForm(request.POST or None)

    if todo_form.is_valid():
        todo_form.save()
        return redirect('todos')
    else:
        todo_form = TodoForm()

    obj_form = { 'todo_form' : todo_form }
    return render_to_response('todos_criar.html', obj_form,
            context_instance=RequestContext(request))

def todo_detalhe(request, todo_id):
    obj = get_object_or_404(Todo, pk=todo_id)
    return render_to_response('todo_detalhe.html', { 'obj' : obj },
            context_instance=RequestContext(request))

def todo_deletar(request, todo_id):
    obj = get_object_or_404(Todo, pk=todo_id)
    if obj:
        obj.delete()
        return redirect(todos)

def todolists(request):
    obj = TodoList.objects.all()
    return render_to_response("todolists.html", {"obj" : obj },
            context_instance=RequestContext(request))

def todolist_criar(request):
    todolist_form = TodoListForm(request.POST or None)

    if todolist_form.is_valid():
        todolist_form.save()
        return redirect('todolists')
    else:
        todolist_form = TodoListForm()

    obj_form = { 'todolist_form' : todolist_form }
    return render_to_response('todolists_criar.html', obj_form,
            context_instance=RequestContext(request))

def todolist_detalhe(request, todolist_id):
    obj = get_object_or_404(TodoList, pk=todolist_id)
    return render_to_response('todolists_detalhe.html', { 'obj' : obj },
            context_instance=RequestContext(request))

def todolist_deletar(request, todolist_id):
    obj = get_object_or_404(Todo, pk=todolist_id)
    if obj:
        obj.delete()
        return redirect('todolists')


def criadores(request):
    obj = Criador.objects.all()
    return render_to_response('criadores.html', { 'obj' : obj }, 
            context_instance=RequestContext(request))

def criador_criar(request):
    criador_form = CriadorForm(request.POST or None)

    if criador_form.is_valid():
        criador_form.save()
        return redirect('criadores')
    else:
        criador_form = CriadorForm()

    obj_form = { 'criador_form' : criador_form }
    return render_to_response('criador_criar.html', obj_form,
            context_instance=RequestContext(request))

def criador_detalhe(request, criador_id):
    obj = get_object_or_404(Criador, pk=criador_id)
    return render_to_response('criador_detalhe.html', { 'obj' : obj },
            context_instance=RequestContext(request))


def criador_deletar(request, criador_id):
    obj = get_object_or_404(Criador, pk=criador_id)
    if obj:
        obj.delete()
        return redirect('criadores') 
