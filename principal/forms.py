# encoding: utf-8
from django.forms import ModelForm
from principal.models import Todo, TodoList, Criador

class TodoForm(ModelForm):
    class Meta:
        model = Todo

class TodoListForm(ModelForm):
    class Meta:
        model = TodoList

class CriadorForm(ModelForm):
    class Meta:
        model = Criador
